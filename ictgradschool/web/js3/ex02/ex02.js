"use strict";

// TODO Car
var car = {
    year: 2007,
    make: "BMW",
    model: "323i",
    bodyType: "Sedan",
    transmission: "Tiptronic",
    odometer: "68,512",
    price: "$16,000"
};

// TODO Music
var musicAlbum = {
    title: 1989,
    artist: "Taylor Swift",
    year: 2014,
    genre: "Pop",
    tracks: ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", 
    "All You Had to Do Was Stay", "Shake It Off", "I Wish You Would", "Bad Blood", 
    "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"],
    toString: function() {
        return "Album: " + "\"" + this.title + "\"" + ", released in " + this.year + " by " + this.artist;
    }
};

//testing if toString() method inside the musicAlbum object works:
console.log(musicAlbum.toString());

//testing output of values in the tracks array inside the musicAlbum object:
for (var i = 0; i < musicAlbum.tracks.length; i++) {
    console.log("Track " + (i+1) + ": " + musicAlbum.tracks[i]);
}