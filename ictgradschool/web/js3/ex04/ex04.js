"use strict";

// TODO Create object prototypes here
function musicAlbum(title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks;
    this.toString = function() {
        return ("Album: " + "\"" + this.title + "\"" + ", released in " + this.year + " by " + this.artist + ". ");
    }
}

var album = new musicAlbum("1989", "Taylor Swift", "2014", "Pop", ["Welcome to New York", 
"Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off", 
"I Wish You Would", "Bad Blood", "Wildest Dreams", "How You Get the Girl", "This Love", "I Know Places", "Clean"]);

var album2 = new musicAlbum("A Deeper Understanding", "The War on Drugs", "2017", "Rock", 
["Up All Night", "Pain", "Holding On", "Strangest Thing", "Knocked Down", "Nothing to Find", "Thinking of a Place", "In Chains", "Clean Living", "You Don't Have to Go"]);

var album3 = new musicAlbum("Singularity", "Jon Hopkins", "2018", "Electronic", ["Singularity", "Emerald Rush", "Neon Pattern Drum", "Everything Connected", "Feel First Life", "C O S M", "Echo Dissolve", "Luminous Beings", "Recovery"]);

// TODO Create functions here
function getAlbums(albumObj, albumObj2, albumObj3) {
    return [albumObj, albumObj2, albumObj3];
}

function printAlbums(albumArray) {
    console.log("Albums:");
    for (var i = 0; i < albumArray.length; i++) {
        console.log(albumArray[i].toString() + "Track listing:");
        for (var property in albumArray[i].tracks) {
            console.log("- " + albumArray[i].tracks[property]);
        }
    }
}

// TODO Complete the program here
var albumsArray = getAlbums(album, album2, album3);
printAlbums(albumsArray);